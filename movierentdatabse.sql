USE [Modeling_Exercise_05]
GO
/****** Object:  Table [dbo].[movies]    Script Date: 9/21/2020 6:50:39 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[movies](
	[movie_id] [int] IDENTITY(1,1) NOT NULL,
	[movie_title] [varchar](50) NOT NULL,
	[movie_director] [varchar](50) NOT NULL,
	[movie_actors] [varchar](50) NOT NULL,
	[movie_genre] [varchar](50) NOT NULL,
	[premier] [varchar](50) NOT NULL,
	[stocked] [varchar](50) NOT NULL,
 CONSTRAINT [PK_movies] PRIMARY KEY CLUSTERED 
(
	[movie_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[rents]    Script Date: 9/21/2020 6:50:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[rents](
	[rent_id] [int] IDENTITY(1,1) NOT NULL,
	[movie_id] [int] NOT NULL,
	[user_id] [int] NOT NULL,
 CONSTRAINT [PK_rents] PRIMARY KEY CLUSTERED 
(
	[rent_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[users]    Script Date: 9/21/2020 6:50:40 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[users](
	[user_id] [int] IDENTITY(1,1) NOT NULL,
	[user_name] [varchar](50) NOT NULL,
	[user_address] [varchar](50) NOT NULL,
	[user_zip] [varchar](50) NOT NULL,
	[user_phone] [varchar](50) NOT NULL,
	[user_email] [varchar](50) NOT NULL,
 CONSTRAINT [PK_users] PRIMARY KEY CLUSTERED 
(
	[user_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[movies] ON 

INSERT [dbo].[movies] ([movie_id], [movie_title], [movie_director], [movie_actors], [movie_genre], [premier], [stocked]) VALUES (1, N'Space Wars', N'Rishi Nolan', N'Jaxx Rawlings, Jemima Phlelps', N'Sci-Fi', N'No', N'Yes')
INSERT [dbo].[movies] ([movie_id], [movie_title], [movie_director], [movie_actors], [movie_genre], [premier], [stocked]) VALUES (2, N'Pulp Non-Fiction', N'Klara Smart', N'Nur Bates, Adriana Munoz', N'Action', N'Yes', N'Yes')
INSERT [dbo].[movies] ([movie_id], [movie_title], [movie_director], [movie_actors], [movie_genre], [premier], [stocked]) VALUES (3, N'eeeee', N'Randy Watson', N'Chantelle Leblanc', N'Comedy', N'No', N'No')
INSERT [dbo].[movies] ([movie_id], [movie_title], [movie_director], [movie_actors], [movie_genre], [premier], [stocked]) VALUES (4, N'King of the Rings', N'Leanna Rutledge', N'Usmaan Milner', N'Fantasy', N'No', N'Yes')
INSERT [dbo].[movies] ([movie_id], [movie_title], [movie_director], [movie_actors], [movie_genre], [premier], [stocked]) VALUES (6, N'Hairy Pottery', N'Eloisa Dominguez', N'Sue Grahm, Martin Ruiz', N'Action', N'No', N'Yes')
INSERT [dbo].[movies] ([movie_id], [movie_title], [movie_director], [movie_actors], [movie_genre], [premier], [stocked]) VALUES (7, N'a', N'bc', N'd', N'e', N'no', N'yes')
SET IDENTITY_INSERT [dbo].[movies] OFF
SET IDENTITY_INSERT [dbo].[rents] ON 

INSERT [dbo].[rents] ([rent_id], [movie_id], [user_id]) VALUES (1, 1, 2)
INSERT [dbo].[rents] ([rent_id], [movie_id], [user_id]) VALUES (2, 1, 2)
INSERT [dbo].[rents] ([rent_id], [movie_id], [user_id]) VALUES (3, 1, 4)
INSERT [dbo].[rents] ([rent_id], [movie_id], [user_id]) VALUES (4, 2, 3)
INSERT [dbo].[rents] ([rent_id], [movie_id], [user_id]) VALUES (5, 2, 4)
INSERT [dbo].[rents] ([rent_id], [movie_id], [user_id]) VALUES (6, 4, 1)
INSERT [dbo].[rents] ([rent_id], [movie_id], [user_id]) VALUES (8, 4, 3)
SET IDENTITY_INSERT [dbo].[rents] OFF
SET IDENTITY_INSERT [dbo].[users] ON 

INSERT [dbo].[users] ([user_id], [user_name], [user_address], [user_zip], [user_phone], [user_email]) VALUES (1, N'Elizabeth Hess', N'932 63rd St,', N'75342', N'503-305-3000', N'coolguy@gmail.com')
INSERT [dbo].[users] ([user_id], [user_name], [user_address], [user_zip], [user_phone], [user_email]) VALUES (2, N'Sonya Armstrong', N'7106 Overlook Rd.', N'87690', N'850-009-3211', N'kittykat@yahoo.com')
INSERT [dbo].[users] ([user_id], [user_name], [user_address], [user_zip], [user_phone], [user_email]) VALUES (3, N'Arnold Mcfarlane', N'781 South New Saddle Ave.', N'23569', N'249-698-3325', N'throwawayemail@gmail.com')
INSERT [dbo].[users] ([user_id], [user_name], [user_address], [user_zip], [user_phone], [user_email]) VALUES (4, N'Everett', N'Glenn', N'13576', N'37160', N'EGlenn@gmail.com')
SET IDENTITY_INSERT [dbo].[users] OFF
ALTER TABLE [dbo].[rents]  WITH CHECK ADD  CONSTRAINT [FK_rents_movies] FOREIGN KEY([movie_id])
REFERENCES [dbo].[movies] ([movie_id])
GO
ALTER TABLE [dbo].[rents] CHECK CONSTRAINT [FK_rents_movies]
GO
ALTER TABLE [dbo].[rents]  WITH CHECK ADD  CONSTRAINT [FK_rents_users] FOREIGN KEY([user_id])
REFERENCES [dbo].[users] ([user_id])
GO
ALTER TABLE [dbo].[rents] CHECK CONSTRAINT [FK_rents_users]
GO
