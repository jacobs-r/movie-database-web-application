﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        SqlConnection conn = new SqlConnection(@"Data Source=ZCM-704200930\SQLEXPRESS;Initial Catalog=Modeling_Exercise_05;Integrated Security=True");
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                var getAllMovies = "SELECT * FROM Movies";
                conn.Open();
                SqlCommand display = conn.CreateCommand();
                display.CommandText = getAllMovies;
                SqlDataAdapter adapter = new SqlDataAdapter(display);
                DataTable table = new DataTable();
                adapter.Fill(table);
                dgvMovies.DataSource = table;
                conn.Close();
            }
            catch (Exception generic)
            {
                MessageBox.Show(generic.Message, "Could not connect to database");
                conn.Close();
            }
        }
    }
}
