﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UsersOutput
{
    public partial class frmUsersOutput : Form
    {
        public frmUsersOutput()
        {
            InitializeComponent();
        }

        private void usersBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.usersBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.modeling_Exercise_05DataSet);

        }

        private void frmUsersOutput_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'modeling_Exercise_05DataSet.users' table. You can move, or remove it, as needed.
            this.usersTableAdapter.Fill(this.modeling_Exercise_05DataSet.users);

        }
    }
}
