﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Windows.Forms;
using Microsoft.VisualBasic;

namespace UsersMaintenance
{
    public partial class frmUsersMaintenance : Form
    {
        public frmUsersMaintenance()
        {
            InitializeComponent();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            Home.frmUsersUpdate frmUsersUpdate = new Home.frmUsersUpdate();
            frmUsersUpdate.Show();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnInsert_Click(object sender, EventArgs e)
        {
            try
            {
                string name = txtName.Text;
                string address = txtAddress.Text;
                decimal zip = nudZIP.Value;
                string phone = txtPhone.Text;
                string email = txtEmail.Text;
                var zipMatch = Regex.Match(zip.ToString(), @"^\d{5}$");
                var phoneMatch = Regex.Match(phone, @"^\d{3}-\d{3}-\d{4}$");
                var emailMatch = Regex.Match(email, @"\w+@\w+.\w[.\w]*$");
                if (name.Length == 0 || address.Length == 0 || !zipMatch.Success || !phoneMatch.Success || !emailMatch.Success)
                {
                    throw new Exception("You must input valid information");
                }
                else
                {
                    try
                    {
                        string query = $@"
                            INSERT INTO users (user_name, user_address, user_zip, user_phone, user_email)
                            VALUES ('{name}', '{address}', '{zip}', '{phone}', '{email}')
                            ";
                        Home.Connection.conn.Open();
                        SqlCommand display = Home.Connection.conn.CreateCommand();
                        display.CommandText = query;
                        SqlDataAdapter adapter = new SqlDataAdapter(display);
                        DataTable table = new DataTable();
                        adapter.Fill(table);
                        Home.Connection.conn.Close();
                        MessageBox.Show("Saved data successfully", "Success", MessageBoxButtons.OK);
                    }
                    catch (Exception generic)
                    {
                        MessageBox.Show(generic.Message, "Could not connect to database");
                        Home.Connection.conn.Close();
                    }
                }
            } catch (Exception generic)
            {
                MessageBox.Show(generic.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                string deleteID = Interaction.InputBox("Enter user ID to delete: ");
                //Ensures inputed ID matches that of one in the database
                Home.Connection.conn.Open();
                SqlCommand maximum = new SqlCommand("SELECT MAX(user_id) FROM users", Home.Connection.conn);
                string maximumID = maximum.ExecuteScalar().ToString();
                Home.Connection.conn.Close();
                if (Convert.ToDecimal(deleteID) <= 0 || Convert.ToDecimal(deleteID) > Convert.ToDecimal(maximumID)) { throw new FormatException("You must input a valid id"); }
                //Deletes from table
                string query = $@"
                            DELETE FROM users
                            WHERE user_id = {Convert.ToDecimal(deleteID)}
                            ";
                Home.Connection.conn.Open();
                SqlCommand display = Home.Connection.conn.CreateCommand();
                display.CommandText = query;
                SqlDataAdapter adapter = new SqlDataAdapter(display);
                DataTable table = new DataTable();
                adapter.Fill(table);
                Home.Connection.conn.Close();
                MessageBox.Show("Deleted data successfully", "Success", MessageBoxButtons.OK);
            }
            catch (Exception generic)
            {
                MessageBox.Show(generic.Message, "Could not connect to database");
                Home.Connection.conn.Close();
            }
        }

        private void txtName_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
