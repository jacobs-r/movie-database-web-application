﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Home
{
    public partial class frmRentsUpdate : Form
    {

        public frmRentsUpdate()
        {
            InitializeComponent();
        }

        private void btnConfirm_Click(object sender, EventArgs e)
        {
            try
            {
                string updateQueryPart = ""; //String placeholder to append query parts of columns and their new values
                decimal movieID = nudUpdateUserID.Value;
                if (movieID != 0) { updateQueryPart += $"movie_id = '{movieID}', "; }
                decimal userID = nudUpdateMovieID.Value;
                if (userID != 0) { updateQueryPart += $"user_id = '{userID}', "; }
                if (movieID < 0 || userID < 0) { throw new ArgumentOutOfRangeException("You must input ids greater than 0");  }
                decimal rentID= nudUpdateID.Value;
                //Ensures inputed ID matches that of one in the database
                Home.Connection.conn.Open();
                SqlCommand maximum = new SqlCommand("SELECT MAX(rent_id) FROM rents", Home.Connection.conn);
                string maximumID = maximum.ExecuteScalar().ToString();
                Home.Connection.conn.Close();
                if (movieID <= 0 || movieID > Convert.ToDecimal(maximumID)) { throw new FormatException("You must input a valid id"); }
                else
                {
                    try
                    {
                        string query = $@"
                            UPDATE rents
                            SET {updateQueryPart.Substring(0, updateQueryPart.Length - 2)}
                            WHERE rent_id = {rentID};
                            ";
                        Home.Connection.conn.Open();
                        SqlCommand display = Home.Connection.conn.CreateCommand();
                        display.CommandText = query;
                        SqlDataAdapter adapter = new SqlDataAdapter(display);
                        DataTable table = new DataTable();
                        adapter.Fill(table);
                        Home.Connection.conn.Close();
                        MessageBox.Show("Saved data successfully", "Success", MessageBoxButtons.OK);
                    }
                    catch (Exception generic)
                    {
                        MessageBox.Show(generic.Message, "Could not connect to database");
                        Home.Connection.conn.Close();
                    }
                }
            }
            catch (FormatException generic)
            {
                MessageBox.Show(generic.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
