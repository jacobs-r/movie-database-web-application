﻿namespace Home
{
    partial class frmMoviesUpdate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chkUpdateAvailable = new System.Windows.Forms.CheckBox();
            this.chkUpdatePremiere = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtUpdateGenre = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtUpdateActors = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtUpdateDirector = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtUpdateTitle = new System.Windows.Forms.TextBox();
            this.nudUpdateID = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.btnConfirm = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudUpdateID)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkUpdateAvailable);
            this.groupBox1.Controls.Add(this.chkUpdatePremiere);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtUpdateGenre);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtUpdateActors);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtUpdateDirector);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtUpdateTitle);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(281, 134);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Movie Summary";
            // 
            // chkUpdateAvailable
            // 
            this.chkUpdateAvailable.AutoSize = true;
            this.chkUpdateAvailable.Location = new System.Drawing.Point(183, 84);
            this.chkUpdateAvailable.Name = "chkUpdateAvailable";
            this.chkUpdateAvailable.Size = new System.Drawing.Size(69, 17);
            this.chkUpdateAvailable.TabIndex = 9;
            this.chkUpdateAvailable.Text = "Available";
            this.chkUpdateAvailable.UseVisualStyleBackColor = true;
            // 
            // chkUpdatePremiere
            // 
            this.chkUpdatePremiere.AutoSize = true;
            this.chkUpdatePremiere.Location = new System.Drawing.Point(183, 33);
            this.chkUpdatePremiere.Name = "chkUpdatePremiere";
            this.chkUpdatePremiere.Size = new System.Drawing.Size(67, 17);
            this.chkUpdatePremiere.TabIndex = 8;
            this.chkUpdatePremiere.Text = "Premiere";
            this.chkUpdatePremiere.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(11, 100);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Genre:";
            // 
            // txtUpdateGenre
            // 
            this.txtUpdateGenre.Location = new System.Drawing.Point(66, 97);
            this.txtUpdateGenre.Name = "txtUpdateGenre";
            this.txtUpdateGenre.Size = new System.Drawing.Size(100, 20);
            this.txtUpdateGenre.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(1, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Actor(s):";
            // 
            // txtUpdateActors
            // 
            this.txtUpdateActors.Location = new System.Drawing.Point(66, 71);
            this.txtUpdateActors.Name = "txtUpdateActors";
            this.txtUpdateActors.Size = new System.Drawing.Size(100, 20);
            this.txtUpdateActors.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(0, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Director:";
            // 
            // txtUpdateDirector
            // 
            this.txtUpdateDirector.Location = new System.Drawing.Point(66, 45);
            this.txtUpdateDirector.Name = "txtUpdateDirector";
            this.txtUpdateDirector.Size = new System.Drawing.Size(100, 20);
            this.txtUpdateDirector.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(20, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Title:";
            // 
            // txtUpdateTitle
            // 
            this.txtUpdateTitle.Location = new System.Drawing.Point(66, 19);
            this.txtUpdateTitle.Name = "txtUpdateTitle";
            this.txtUpdateTitle.Size = new System.Drawing.Size(100, 20);
            this.txtUpdateTitle.TabIndex = 0;
            // 
            // nudUpdateID
            // 
            this.nudUpdateID.Location = new System.Drawing.Point(118, 152);
            this.nudUpdateID.Name = "nudUpdateID";
            this.nudUpdateID.Size = new System.Drawing.Size(163, 20);
            this.nudUpdateID.TabIndex = 20;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(28, 154);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(84, 13);
            this.label6.TabIndex = 19;
            this.label6.Text = "ID to Update:";
            // 
            // btnConfirm
            // 
            this.btnConfirm.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.btnConfirm.Location = new System.Drawing.Point(12, 178);
            this.btnConfirm.Name = "btnConfirm";
            this.btnConfirm.Size = new System.Drawing.Size(275, 46);
            this.btnConfirm.TabIndex = 18;
            this.btnConfirm.Text = "Confirm";
            this.btnConfirm.UseVisualStyleBackColor = false;
            this.btnConfirm.Click += new System.EventHandler(this.btnConfirm_Click);
            // 
            // frmMoviesUpdate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(306, 233);
            this.Controls.Add(this.nudUpdateID);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btnConfirm);
            this.Controls.Add(this.groupBox1);
            this.Name = "frmMoviesUpdate";
            this.Text = "Movies Update";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudUpdateID)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox chkUpdateAvailable;
        private System.Windows.Forms.CheckBox chkUpdatePremiere;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtUpdateGenre;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtUpdateActors;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtUpdateDirector;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtUpdateTitle;
        private System.Windows.Forms.NumericUpDown nudUpdateID;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnConfirm;
    }
}