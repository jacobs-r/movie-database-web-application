﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using System.Data.SqlClient;

namespace Home
{
    public partial class frmHome : Form
    {

        public frmHome()
        {
            InitializeComponent();
        }

        private void listOfUsersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmOutput frmOutput = new frmOutput("SELECT * FROM Users");
            frmOutput.Show();
        }

        private void listOfMoviesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmOutput frmOutput = new frmOutput("SELECT * FROM Movies");
            frmOutput.Show();
        }

        private void moviesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MoviesMaintenance.frmMoviesMaintenance frmMoviesMaintenance = new MoviesMaintenance.frmMoviesMaintenance();
            frmMoviesMaintenance.Show();
        }

        private void usersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UsersMaintenance.frmUsersMaintenance frmUsersMaintenance = new UsersMaintenance.frmUsersMaintenance();
            frmUsersMaintenance.Show();
        }

        private void rentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RentsMaintenance.frmRentsMaintenance frmRentsMaintenance = new RentsMaintenance.frmRentsMaintenance();
            frmRentsMaintenance.Show();
        }

        private void allAvailaleMoviesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmOutput frmOutput = new frmOutput("SELECT * FROM Movies WHERE stocked = 'Yes'");
            frmOutput.Show();
        }

        private void allRentedMoviesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmOutput frmOutput = new frmOutput(@"
                SELECT movie_title AS Rented Movies
                FROM Movies m
                INNER JOIN rents r
                ON m.movie_id = r.movie_id
                GROUP BY movie_title
            ");
            frmOutput.Show();
        }

        private void listOfRentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmOutput frmOutput = new frmOutput("SELECT * FROM rents");
            frmOutput.Show();
        }

        private void titleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string searchTerm = Interaction.InputBox("Enter title to search for: ");
            //Checks if term is in database at a;;
            Home.Connection.conn.Open();
            SqlCommand search = new SqlCommand($"SELECT movie_id FROM movies WHERE movie_title = '{searchTerm}'", Home.Connection.conn);
            var returnedValue = search.ExecuteScalar();
            Home.Connection.conn.Close();
            if (returnedValue == null) { MessageBox.Show("Movie not found", "error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation); }
            else
            {
                frmOutput frmOutput = new frmOutput($"SELECT * FROM movies WHERE movie_title = '{searchTerm}'");
                frmOutput.Show();
            }
        }

        private void directorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string searchTerm = Interaction.InputBox("Enter director to search for: ");
            //Checks if term is in database at a;;
            Home.Connection.conn.Open();
            SqlCommand search = new SqlCommand($"SELECT movie_id FROM movies WHERE movie_director = '{searchTerm}'", Home.Connection.conn);
            var returnedValue = search.ExecuteScalar();
            Home.Connection.conn.Close();
            if (returnedValue == null) { MessageBox.Show("Movie not found", "error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation); }
            else
            {
                frmOutput frmOutput = new frmOutput($"SELECT * FROM movies WHERE movie_director = '{searchTerm}'");
                frmOutput.Show();
            }
        }

        private void genreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string searchTerm = Interaction.InputBox("Enter genre to search for: ");
            //Checks if term is in database at a;;
            Home.Connection.conn.Open();
            SqlCommand search = new SqlCommand($"SELECT movie_id FROM movies WHERE movie_genre = '{searchTerm}'", Home.Connection.conn);
            var returnedValue = search.ExecuteScalar();
            Home.Connection.conn.Close();
            if (returnedValue == null) { MessageBox.Show("Movie not found", "error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation); }
            else
            {
                frmOutput frmOutput = new frmOutput($"SELECT * FROM movies WHERE movie_genre = '{searchTerm}'");
                frmOutput.Show();
            }
        }
    }
}
