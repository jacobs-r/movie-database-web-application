﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Home
{
    public partial class frmMoviesUpdate : Form
    {

        public frmMoviesUpdate()
        {
            InitializeComponent();
        }

        private void btnConfirm_Click(object sender, EventArgs e)
        {
            try
            {
                string updateQueryPart = ""; //String placeholder to append query parts of columns and their new values
                string title = txtUpdateTitle.Text;
                if (title.Length != 0) { updateQueryPart += $"movie_title = '{title}', "; }
                string director = txtUpdateDirector.Text;
                if (director.Length != 0) { updateQueryPart += $"movie_director = '{director}', "; }
                string actors = txtUpdateActors.Text;
                if (actors.Length != 0) { updateQueryPart += $"movie_actors = '{actors}', "; }
                string genre = txtUpdateGenre.Text;
                if (genre.Length != 0) { updateQueryPart += $"movie_genre = '{genre}', "; }
                string premiere = "No";
                string available = "No";
                if (chkUpdateAvailable.Checked) { available = "Yes"; }
                if (chkUpdatePremiere.Checked) { premiere = "Yes"; }
                updateQueryPart += $"premier = '{premiere}', ";
                updateQueryPart += $"stocked = '{available}', ";
                decimal movieID = nudUpdateID.Value;
                //Ensures inputed ID matches that of one in the database
                Home.Connection.conn.Open();
                SqlCommand maximum = new SqlCommand("SELECT MAX(user_id) FROM users", Home.Connection.conn);
                string maximumID = maximum.ExecuteScalar().ToString();
                Home.Connection.conn.Close();
                if (movieID <= 0 || movieID> Convert.ToDecimal(maximumID)) { throw new FormatException("You must input a valid id"); }
                else
                {
                    try
                    {
                        string query = $@"
                            UPDATE movies
                            SET {updateQueryPart.Substring(0, updateQueryPart.Length - 2)}
                            WHERE movie_id = {movieID};
                            ";
                        Home.Connection.conn.Open();
                        SqlCommand display = Home.Connection.conn.CreateCommand();
                        display.CommandText = query;
                        SqlDataAdapter adapter = new SqlDataAdapter(display);
                        DataTable table = new DataTable();
                        adapter.Fill(table);
                        Home.Connection.conn.Close();
                        MessageBox.Show("Saved data successfully", "Success", MessageBoxButtons.OK);
                    }
                    catch (Exception generic)
                    {
                        MessageBox.Show(generic.Message, "Could not connect to database");
                        Home.Connection.conn.Close();
                    }
                }
            }
            catch (FormatException generic)
            {
                MessageBox.Show(generic.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
