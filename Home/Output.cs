﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Home
{
    public partial class frmOutput : Form
    {
        string query = "";
        public frmOutput(string transfer)
        {
            query = transfer;
            InitializeComponent();
        }

        private void MoviesOutput_Load(object sender, EventArgs e)
        {
            try
            {
                Home.Connection.conn.Open();
                SqlCommand display = Home.Connection.conn.CreateCommand();
                display.CommandText = query;
                SqlDataAdapter adapter = new SqlDataAdapter(display);
                DataTable table = new DataTable();
                adapter.Fill(table);
                dgvOutput.DataSource = table;
                Home.Connection.conn.Close();
            }
            catch (Exception generic)
            {
                MessageBox.Show(generic.Message, "Could not connect to database");
                Home.Connection.conn.Close();
            }
        }
    }
}
