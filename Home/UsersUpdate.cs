﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Home
{
    public partial class frmUsersUpdate : Form
    {

        public frmUsersUpdate()
        {
            InitializeComponent();
        }

        private void btnConfirm_Click(object sender, EventArgs e)
        {
            try
            {
                string updateQueryPart = ""; //String placeholder to append query parts of columns and their new values
                string name = txtUpdateName.Text;
                if (name.Length != 0) { updateQueryPart += $"user_name = {name}, "; }
                string address = txtUpdateAddress.Text;
                if (address.Length != 0) { updateQueryPart += $"user_address = {address}, "; }
                decimal zip = nudUpdateZIP.Value;
                var zipMatch = Regex.Match(zip.ToString(), @"^\d{5}$");
                if (zipMatch.Success && zip != 0) { updateQueryPart += $"user_zip = {zip}, "; }
                string phone = txtUpdatePhone.Text;
                var phoneMatch = Regex.Match(phone, @"^\d{3}-\d{3}-\d{4}$");
                if (phoneMatch.Success && phone.Length != 0) { updateQueryPart += $"user_phone = {phone}, "; }
                string email = txtUpdateEmail.Text;
                var emailMatch = Regex.Match(email, @"\w+@\w+.\w[.\w]*$");
                if (emailMatch.Success && email.Length != 0) { updateQueryPart += $"user_email = {email}, "; }
                decimal userID = nudUpdateID.Value;
                //Ensures inputed ID matches that of one in the database
                Home.Connection.conn.Open();
                SqlCommand maximum = new SqlCommand("SELECT MAX(user_id) FROM users", Home.Connection.conn);
                string maximumID = maximum.ExecuteScalar().ToString();
                Home.Connection.conn.Close();
                if (userID <= 0 || userID > Convert.ToDecimal(maximumID)) { throw new FormatException("You must input a valid id"); }
                //Ensures that inputed information is correctly formated
                if ((!zipMatch.Success && zip != 0) || (!phoneMatch.Success && phone.Length != 0) || (!emailMatch.Success && email.Length != 0))
                {
                    throw new FormatException("If you update data, it must be valid");
                }
                else
                {
                    try
                    {
                        string query = $@"
                            UPDATE users
                            SET {updateQueryPart.Substring(0, updateQueryPart.Length - 2)}
                            WHERE user_id = {userID};
                            ";
                        Home.Connection.conn.Open();
                        SqlCommand display = Home.Connection.conn.CreateCommand();
                        display.CommandText = query;
                        SqlDataAdapter adapter = new SqlDataAdapter(display);
                        DataTable table = new DataTable();
                        adapter.Fill(table);
                        Home.Connection.conn.Close();
                        MessageBox.Show("Saved data successfully", "Success", MessageBoxButtons.OK);
                    }
                    catch (Exception generic)
                    {
                        MessageBox.Show(generic.Message, "Could not connect to database");
                        Home.Connection.conn.Close();
                    }
                }
            }
            catch (FormatException generic)
            {
                MessageBox.Show(generic.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
