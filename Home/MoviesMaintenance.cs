﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;
using Microsoft.VisualBasic;


namespace MoviesMaintenance
{
    public partial class frmMoviesMaintenance : Form
    {
        public frmMoviesMaintenance()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            Home.frmMoviesUpdate frmMoviesUpdate = new Home.frmMoviesUpdate();
            frmMoviesUpdate.Show();
        }

        private void btnInsert_Click(object sender, EventArgs e)
        {
            try
            {
                string title = txtTitle.Text;
                string director = txtDirector.Text;
                string actors = txtActors.Text;
                string genre = txtGenre.Text;
                string premiere = "No";
                string available = "No";
                if (chkAvailable.Checked) { available = "Yes"; }
                if (chkPremiere.Checked) { premiere = "Yes"; }
                if (title.Length == 0 || director.Length == 0 || actors.Length == 0 || genre.Length == 0)
                {
                    throw new Exception("You must input valid information");
                }
                else
                {
                    try
                    {
                        string query = $@"
                            INSERT INTO movies (movie_title, movie_director, movie_actors, movie_genre, premier, stocked)
                            VALUES ('{title}', '{director}', '{actors}', '{genre}', '{premiere}', '{available}')
                            ";
                        Home.Connection.conn.Open();
                        SqlCommand display = Home.Connection.conn.CreateCommand();
                        display.CommandText = query;
                        SqlDataAdapter adapter = new SqlDataAdapter(display);
                        DataTable table = new DataTable();
                        adapter.Fill(table);
                        Home.Connection.conn.Close();
                        MessageBox.Show("Saved data successfully", "Success", MessageBoxButtons.OK);
                    }
                    catch (Exception generic)
                    {
                        MessageBox.Show(generic.Message, "Could not connect to database");
                        Home.Connection.conn.Close();
                    }
                }
            }
            catch (Exception generic)
            {
                MessageBox.Show(generic.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                string deleteID = Interaction.InputBox("Enter movie ID to delete: ");
                //Ensures inputed ID matches that of one in the database
                Home.Connection.conn.Open();
                SqlCommand maximum = new SqlCommand("SELECT MAX(movie_id) FROM movies", Home.Connection.conn);
                string maximumID = maximum.ExecuteScalar().ToString();
                Home.Connection.conn.Close();
                if (Convert.ToDecimal(deleteID) <= 0 || Convert.ToDecimal(deleteID) > Convert.ToDecimal(maximumID)) { throw new FormatException("You must input a valid id"); }
                //Deletes from table
                string query = $@"
                            DELETE FROM movies
                            WHERE movie_id = {Convert.ToDecimal(deleteID)}
                            ";
                Home.Connection.conn.Open();
                SqlCommand display = Home.Connection.conn.CreateCommand();
                display.CommandText = query;
                SqlDataAdapter adapter = new SqlDataAdapter(display);
                DataTable table = new DataTable();
                adapter.Fill(table);
                Home.Connection.conn.Close();
                MessageBox.Show("Deleted data successfully", "Success", MessageBoxButtons.OK);
            }
            catch (Exception generic)
            {
                MessageBox.Show(generic.Message, "Could not connect to database");
                Home.Connection.conn.Close();
            }
        }
    }
}
