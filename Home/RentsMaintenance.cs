﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using Microsoft.VisualBasic;

namespace RentsMaintenance
{
    public partial class frmRentsMaintenance : Form
    {

        public frmRentsMaintenance()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            Home.frmRentsUpdate frmRentsUpdate = new Home.frmRentsUpdate();
            frmRentsUpdate.Show();
        }

        private void btnInsert_Click(object sender, EventArgs e)
        {
            try
            {
                decimal userID = nudUserID.Value;
                decimal movieID = nudMovieID.Value;
                if (userID <= 0 || movieID <= 0) { throw new ArgumentOutOfRangeException("You must input IDs greater than 0"); }
                else
                {
                    try
                    {
                        string query = $@"
                            INSERT INTO rents (movie_id, user_id)
                            VALUES ('{movieID}', '{userID}')
                            ";
                        Home.Connection.conn.Open();
                        SqlCommand display = Home.Connection.conn.CreateCommand();
                        display.CommandText = query;
                        SqlDataAdapter adapter = new SqlDataAdapter(display);
                        DataTable table = new DataTable();
                        adapter.Fill(table);
                        Home.Connection.conn.Close();
                        MessageBox.Show("Saved data successfully", "Success", MessageBoxButtons.OK);
                    }
                    catch (Exception generic)
                    {
                        MessageBox.Show(generic.Message, "Could not connect to database");
                        Home.Connection.conn.Close();
                    }
                }
            }
            catch (Exception generic)
            {
                MessageBox.Show(generic.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                string deleteID = Interaction.InputBox("Enter rent ID to delete: ");
                //Ensures inputed ID matches that of one in the database
                Home.Connection.conn.Open();
                SqlCommand maximum = new SqlCommand("SELECT MAX(rent_id) FROM rents", Home.Connection.conn);
                string maximumID = maximum.ExecuteScalar().ToString();
                Home.Connection.conn.Close();
                if (Convert.ToDecimal(deleteID) <= 0 || Convert.ToDecimal(deleteID) > Convert.ToDecimal(maximumID)) { throw new FormatException("You must input a valid id"); }
                //Deletes from table
                string query = $@"
                            DELETE FROM rents
                            WHERE rent_id = {Convert.ToDecimal(deleteID)}
                            ";
                Home.Connection.conn.Open();
                SqlCommand display = Home.Connection.conn.CreateCommand();
                display.CommandText = query;
                SqlDataAdapter adapter = new SqlDataAdapter(display);
                DataTable table = new DataTable();
                adapter.Fill(table);
                Home.Connection.conn.Close();
                MessageBox.Show("Deleted data successfully", "Success", MessageBoxButtons.OK);
            }
            catch (Exception generic)
            {
                MessageBox.Show(generic.Message, "Could not connect to database");
                Home.Connection.conn.Close();
            }
        }
    }
}
